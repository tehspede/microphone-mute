const { app, Tray, globalShortcut } = require('electron');
const { execString } = require('applescript');
const { join } = require('path');

let tray = null;
let muted = false;

const isMuted = (callback) => execString('input volume of (get volume settings)', (_, val) => {
  callback(!val);
});

const setMute = (mute) => execString(`
  set level to ${mute ? 0 : 90}
  set volume input volume level
`);

const getPath = (file) => {
  const assetPath = app.isPackaged ? `${process.resourcesPath}/..` : '';

  return join(assetPath, file);
}

const getMicImage = (muted) => `mic${muted ? 'Off' : ''}Template.png`;

const updateImage = (muted) => {
  tray.setImage(getPath(getMicImage(muted)));
};

const onTrayClick = () => {
  muted = !muted;

  setMute(muted);
  updateImage(muted);
};

setInterval(() => isMuted((val) => {
  if (muted === val) {
    return;
  }

  muted = val;
  updateImage(muted);
}), 1000);

app.on('ready', () => {
    tray = new Tray(getPath(getMicImage(muted)));
    tray.on('click', onTrayClick);
    tray.on('right-click', app.quit);

    globalShortcut.register('Shift+Command+M', onTrayClick);
});
